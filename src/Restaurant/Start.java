package Restaurant;


import java.util.*;

class CompareDish implements Comparator<Dish>{
    @Override
    public int compare(Dish o1, Dish o2){

        return  Double.compare(o1.price , o2.price);
    }
}
class CompareDish2 implements Comparator<Dish>{
    @Override
    public int compare(Dish o1, Dish o2){

        return  Double.compare(o1.weight, o2.weight);
    }
}
public class Start {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in );
        /////создание каталога блюд
       List<Dish> list = new ArrayList<>();

        Dish dish1  = new Dish(350, 0.500, "Cesar", "salat");
        list.add(dish1);
        Dish dish2  = new Dish(1650, 1.000, "Chicken & potatoes", "second");
        list.add(dish2);
        Dish dish3  = new Dish(1000, 1.500, "Turtle soup", "first");
        list.add(dish3);
        System.out.println("sored " + list);

        Collections.sort(list, new Comparator<Dish>() {
            @Override
            public int compare(Dish dish1, Dish dish2) {
                return dish1.getName().compareTo(dish2.getName());
            }
        });
        int a = 5;
        int b = 10;
        if(a==b){

        }
        ///Его вывод
        list.forEach(Dish -> System.out.println("- Price: "+Dish.price  + "Weight: " + Dish.weight + "Name: " + Dish.getName() + "Type: "+ Dish.getType() ));

        ///Функционал
        System.out.println("Enter number of Work:");
        int enter = sc.nextInt();

        switch (enter){
            case(1):
                list.add(new Dish(sc.nextDouble(), sc.nextDouble(), sc.next(), sc.next()));
                break;
            case(2):
                list.remove(sc.nextInt());
                break;
            case(3):
                list.get(sc.nextInt()).setName(sc.nextLine());
                break;
            case(4):
                list.sort(new CompareDish());
            default:
                break;
        }
        list.forEach(Dish -> System.out.println("- Price: "+Dish.price  + " Weight: " + Dish.weight + " Name: " + Dish.getName() + " Type: "+ Dish.getType() ));
    }


}
