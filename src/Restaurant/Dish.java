package Restaurant;

import java.util.ArrayList;

public class Dish extends Weight {
    private String name;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Dish(double price, double weight, String name, String type) {
        super(price, weight);
        this.name = name;
        this.type = type;
    }


}
