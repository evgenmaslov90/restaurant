package Workers;

public class Student extends User {
    private int year;
    private int stipendia;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getStipendia() {
        return stipendia;
    }

    public void setStipendia(int stipendia) {
        this.stipendia = stipendia;
    }
}
