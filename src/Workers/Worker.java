package Workers;

public class Worker extends User implements Human {
    private int salary;


    public int getSalary() {
        return salary;
    }

    public int setSalary(int salary) {
        this.salary = salary;
        return salary;
    }


}
