package Workers;

public abstract class User {
    protected String name;
    protected int age;

    public String setName(String name) {
        this.name = name;
        return name;
    }

    public int setAge(int age) {
        this.age = age;
        return age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
